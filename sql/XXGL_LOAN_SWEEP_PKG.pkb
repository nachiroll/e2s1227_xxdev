CREATE OR REPLACE PACKAGE BODY APPS.XXGL_LOAN_SWEEP_PKG AS
  ----------------------------------------------------------------------
  /*
   Package Name    : XXGL_LOAN_SWEEP_PKG
   Author's Name   : Nattanan P.
   RICEW Object ID : XXGL_LOAN_SWEEP_PKG

   Date Written    : 30-Jan-2019

   Concurrent Program : XXGL - Loan Sweep

   Description :  This Script Creates the Custom Package Spec
                  for provide XXGL - Loan Sweep to TMS by CSV format

   Maintenance History :

   Date        Issue# Name                 Remarks
   ----------- ------ -------------------- ------------------------------
   30-Jan-2019      1 Nattanan P.          Initial Development.
  */

PROCEDURE main_prc(o_errbuf  out varchar2,
                   o_retcode out varchar2,
                   --
                   p_user_id      in number,
                   p_resp_id      in number,
                   p_resp_app_id  in number,
                   p_gl_date_from in varchar2,                   
                   p_gl_date_to in varchar2,
                   p_prog_short_name in varchar2,
                   p_file_type_frm in varchar2,
                   p_file_type_to in varchar2,
                   p_file_name in varchar2) IS
                                      
cursor c_gl (p_gl_dt_frm date, p_gl_dt_to date) is
select src.user_je_source_name src_name
       ,cat.user_je_category_name cat_name
       ,gjb.name bat_name
       ,gjh.name jv_name
       ,gjl.je_line_num jv_line_num
       ,gjh.default_effective_date gl_date
       ,gcc.segment1
       ,gcc.segment2
       ,gcc.segment3
       ,gcc.segment4
       ,gcc.segment5
       ,gcc.segment6
       ,gcc.segment7
       ,gjl.accounted_dr
       ,gjl.accounted_cr
       ,leg.currency_code leg_cur
from gl_je_batches gjb
    ,gl_je_headers gjh
    ,gl_je_sources src
    ,gl_je_categories cat
    ,gl_ledgers leg
    ,gl_je_lines gjl
    ,gl_code_combinations gcc
where gjb.je_batch_id = gjh.je_batch_id
and gjh.je_source = src.je_source_name
and gjh.je_category = cat.je_category_name
and  gjh.ledger_id = leg.ledger_id
and gjl.je_header_id = gjh.je_header_id
and gjl.code_combination_id = gcc.code_combination_id
and src.user_je_source_name = 'SWEEP'
and gjh.status = 'P'
and gcc.segment4 = '1120130'
and gjh.default_effective_date between p_gl_dt_frm and p_gl_dt_frm
order by gjh.default_effective_date, gjb.name, gjl.je_line_num;                 

l_dr_amt varchar2(100);
l_cr_amt varchar2(100);

l_gl_dt_frm date;
l_gl_dt_to date;
l_err_cnt number := 0;
l_err_line_cnt number;
l_cvt_gl_date varchar2(25);
l_cnt_rec number := 0;
l_err_message varchar2(500) := null;
l_completed_rec number := 0;

BEGIN

    write_log(' - Start XXGL_LOAN_SWEEP_PKG.main_prc');                
    
    -- Setup Initialize
    -------------------------------------------------------------------------
    apps_initial(l_user_id     => p_user_id, --in number,
                 l_resp_id     => p_resp_id, --in number,
                 l_resp_app_id => p_resp_app_id --in number
                 );

    l_gl_dt_frm := p_gl_date_from;
    l_gl_dt_to := p_gl_date_to;

    --Write Header Column
    write_output('GL Source'|| ',' ||
                 'GL Category'|| ',' ||
                 'GL Batch Name'|| ',' ||
                 'GL Journal Name'|| ',' ||
                 'Journal line number'|| ',' ||
                 'GL Date'|| ',' ||
                 'Segment1'|| ',' ||
                 'Segment2'|| ',' ||
                 'Segment3'|| ',' ||
                 'Segment4'|| ',' ||
                 'Segment5'|| ',' ||
                 'Segment6'|| ',' ||
                 'Segment7'|| ',' ||
                 'Account_Amount_Dr'|| ',' ||
                 'Account_Amount_Cr'|| ',' ||
                 'Ledger_currency');            
                               
    for rec_gl in c_gl (l_gl_dt_frm, l_gl_dt_to) loop
        
        l_cnt_rec := l_cnt_rec + 1;
        l_err_line_cnt := 0;
                            
        --Validation                                            
        if rec_gl.gl_date is not null then
                                                                
            begin
            select to_char(rec_gl.gl_date, 'DD-Mon-YYYY')
                into l_cvt_gl_date
            from dual;                
            exception when others then
                l_cvt_gl_date := null;   
                l_err_line_cnt := l_err_line_cnt + 1;
                write_log(rec_gl.gl_date||' Error:- Cannot convert conversion gl date to format DD-Mon-YYYY');              
            end;
        end if;
                            
        begin
        select nvl(substr(round(rec_gl.accounted_dr,2), 1, instr(round(rec_gl.accounted_dr,2),'.')-1),0)||'.'||
               rpad(nvl(substr(round(rec_gl.accounted_dr,2), instr(round(rec_gl.accounted_dr,2),'.')+1),0), 2, 0) dr_amt
            into l_dr_amt
        from dual;
        exception when others then
            l_dr_amt := '0.00';                    
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_gl.accounted_dr||' Error:- Cannot convert number format N.NN');
        end;
                
        begin
        select nvl(substr(round(rec_gl.accounted_cr,2), 1, instr(round(rec_gl.accounted_cr,2),'.')-1),0)||'.'||
               rpad(nvl(substr(round(rec_gl.accounted_cr,2), instr(round(rec_gl.accounted_cr,2),'.')+1),0), 2, 0) dr_amt
            into l_cr_amt        
        from dual;
        exception when others then
            l_cr_amt := '0.00';                    
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_gl.accounted_cr||' Error:- Cannot convert number format N.NN');
        end;
                        
        if l_err_line_cnt = 0 then
            --Write Header Column
            write_output(substr(rec_gl.src_name,1,25)|| ',' ||
                         substr(rec_gl.cat_name,1,25)|| ',' ||
                         substr(rec_gl.bat_name,1,100)|| ',' ||
                         substr(rec_gl.jv_name,1,100)|| ',' ||
                         rec_gl.jv_line_num|| ',' ||
                         substr(l_cvt_gl_date,1,11)|| ',' ||
                         substr(rec_gl.segment1,1,3)|| ',' ||
                         substr(rec_gl.segment2,1,3)|| ',' ||
                         substr(rec_gl.segment3,1,5)|| ',' ||
                         substr(rec_gl.segment4,1,7)|| ',' ||
                         substr(rec_gl.segment5,1,4)|| ',' ||
                         substr(rec_gl.segment6,1,3)|| ',' ||
                         substr(rec_gl.segment7,1,5)|| ',' ||
                         l_dr_amt|| ',' ||
                         l_cr_amt|| ',' ||
                         substr(rec_gl.leg_cur,1,3));                        
            l_completed_rec := l_completed_rec + 1;
        else
            l_err_cnt := l_err_cnt + 1;                
        end if;
                                                              
    end loop;                            
                                                                                    
    write_log('Total records is '||l_cnt_rec);    
    write_log('Completed records is '||l_completed_rec);
    write_log('Rejected records is '||l_err_cnt);
            
    write_log(' - End XXGL_LOAN_SWEEP_PKG.main_prc');
    
END main_prc;

PROCEDURE apps_initial(l_user_id     in number,
                       l_resp_id     in number,
                       l_resp_app_id in number) is
--
BEGIN
write_log(' - Run Application Initialize.');

APPS.FND_GLOBAL.apps_initialize(user_id      => l_user_id, --in number,
                                resp_id      => l_resp_id, --in number,
                                resp_appl_id => l_resp_app_id --in number,
                                );

EXCEPTION WHEN OTHERS THEN
    write_log(' - ***** Error While Application Initialize *****');
    write_log(SQLERRM);
END apps_initial;

PROCEDURE write_output(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.output,l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_output : ' || sqlerrm);         
END write_output;

PROCEDURE write_log(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.log,to_char(sysdate, 'HH24:MI:SS') || '  ' || l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_log : ' || sqlerrm);         
END WRITE_LOG;

PROCEDURE submit_prc(o_errbuf  out varchar2,
                     o_retcode out varchar2,
                     --
                     p_user_id      in number,
                     p_resp_id      in number,
                     p_resp_app_id  in number,
                     p_gl_date_from in varchar2,                   
                     p_gl_date_to in varchar2,
                     p_prog_short_name in varchar2,
                     p_file_type_frm in varchar2,
                     p_file_type_to in varchar2,
                     p_file_name in varchar2) IS
l_request_id number;
l_request_data varchar2(100);
l_appl_code varchar2(150) := 'XXDEV';
l_prog_code varchar2(150) := 'XXGL_TMS_MV_CSVFILE';
l_sub_request_id_out number;
l_sub_request_id_csv number;
l_request_count number;
l_phase_out varchar2(50);
l_status_out varchar2(50);
l_dev_phase_out varchar2(50);
l_dev_status_out varchar2(50);
l_message_out varchar2(50);
l_req_return_status_out boolean;
l_normal_count number := 0;
l_warning_count number := 0;  
l_error_count number := 0;
l_sub_requests fnd_concurrent.requests_tab_type;
i number;
l_actual_start_date date;
l_actual_completion_date date;
l_action_message VARCHAR2(200);

l_gl_dt_frm date;
l_gl_dt_to date;
l_err_cnt number :=0;
l_cnt_data number := 0;
l_gl_end_dt varchar2(50);

BEGIN
    write_log(' - Start XXGL_LOAN_SWEEP_PKG.submit_prc');
    
    if p_gl_date_from is not null and p_gl_date_to is null then
        l_gl_dt_frm := null;
        l_gl_dt_to := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- GL Date To is null');
    elsif p_gl_date_from is null and p_gl_date_to is not null then
        l_gl_dt_frm := null;
        l_gl_dt_to := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- GL Date From is null');
    elsif p_gl_date_from is null and p_gl_date_to is null then
        begin
        select last_day(add_months(trunc(sysdate),-2))+1 gl_dt_frm, last_day(add_months(trunc(sysdate),-1)) gl_dt_to
            into l_gl_dt_frm, l_gl_dt_to
        from dual;
        exception when others then
           l_gl_dt_frm := null;
           l_gl_dt_to := null;
           l_err_cnt := l_err_cnt + 1;
           write_log('Error:- Cannot get gl date from - to');
        end;
    elsif p_gl_date_from is not null and p_gl_date_to is not null then
        begin
        select trunc(to_date(p_gl_date_from, 'YYYY/MM/DD HH24:MI:SS')) gl_dt_frm, trunc(to_date(p_gl_date_to, 'YYYY/MM/DD HH24:MI:SS')) gl_dt_to
            into l_gl_dt_frm, l_gl_dt_to
        from dual;
        exception when others then
            l_gl_dt_frm := null;
            l_gl_dt_to := null;
            l_err_cnt := l_err_cnt + 1;
            write_log('Error:- Cannot convert parameter gl date from - to format');
        end;
    end if;
    
    begin
    select to_char(to_date(l_gl_dt_to),'YYYY/MM/DD HH24:MI:SS')
        into l_gl_end_dt
    from dual;
    exception when others then
        l_gl_end_dt := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- Cannot convert gl end date format');
    end;
    
    begin
    select count(*)
        into l_cnt_data
    from gl_je_batches gjb
        ,gl_je_headers gjh
        ,gl_je_sources src
        ,gl_je_categories cat
        ,gl_ledgers leg
        ,gl_je_lines gjl
        ,gl_code_combinations gcc
    where gjb.je_batch_id = gjh.je_batch_id
    and gjh.je_source = src.je_source_name
    and gjh.je_category = cat.je_category_name
    and  gjh.ledger_id = leg.ledger_id
    and gjl.je_header_id = gjh.je_header_id
    and gjl.code_combination_id = gcc.code_combination_id
    and src.user_je_source_name = 'SWEEP'
    and gjh.status = 'P'
    and gcc.segment4 = '1120130'
    and gjh.default_effective_date between l_gl_dt_frm and l_gl_dt_frm;
    exception when others then
        l_cnt_data := 0;
    end;
    
    if l_cnt_data = 0 then
        write_log(' - ***** No data found *****');
    else
        if l_err_cnt = 0 then         
            l_request_id :=  fnd_global.conc_request_id;
            l_request_data := fnd_conc_global.request_data;
            l_request_count := 0;        
                
            if (l_request_data is null) then
                
                write_log('DEBUG: Program submition list');
                write_log(Rpad('Program Name', 50)||' '||Rpad('Request ID', 15));
                write_log(Rpad('-',50, '-')||' '||Rpad('-',15, '-'));    
            
                l_request_count := l_request_count + 1;
                             
                l_sub_request_id_out := fnd_request.submit_request(application => l_appl_code,
                                                                   program     => p_prog_short_name,
                                                                   description => null,
                                                                   start_time  => null,
                                                                   sub_request => true,
                                                                   argument1   => p_user_id,
                                                                   argument2   => p_resp_id,
                                                                   argument3   => p_resp_app_id,
                                                                   argument4   => l_gl_dt_frm,
                                                                   argument5   => l_gl_dt_to,
                                                                   argument6   => p_prog_short_name,
                                                                   argument7   => p_file_type_frm,
                                                                   argument8   => p_file_type_to,
                                                                   argument9   => p_file_name);
            
                commit;
              
                if (l_sub_request_id_out = 0) then
                    write_log(Rpad(p_prog_short_name, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                else
                    write_log(Rpad(p_prog_short_name, 50)||' '||l_sub_request_id_out);          
                end if;        
                    
                if l_sub_request_id_out is not null then
                                                                                                                                                                         
                    l_request_count := l_request_count + 1;
                            
                    l_sub_request_id_csv := fnd_request.submit_request(application => l_appl_code,
                                                                       program     => l_prog_code,
                                                                       description => null,
                                                                       start_time  => null,
                                                                       sub_request => true,
                                                                       argument1   => l_sub_request_id_out,
                                                                       argument2   => l_gl_end_dt,
                                                                       argument3   => p_prog_short_name,
                                                                       argument4   => p_file_type_frm,
                                                                       argument5   => p_file_type_to,
                                                                       argument6   => p_file_name);
                    commit;
                            
                    if (l_sub_request_id_csv = 0) then
                        write_log(Rpad(l_prog_code, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                    else
                        write_log(Rpad(l_prog_code, 50)||' '||l_sub_request_id_csv);          
                    end if;
                    
                end if;            

                l_request_count := Nvl(l_request_count, 0);
                fnd_conc_global.set_req_globals(conc_status  => 'PAUSED',request_data => To_char(l_request_count));
            else
                write_log(' ');
                write_log('Program Status');
                write_log(Rpad('-',118,'-'));
                write_log(Rpad('Request ID', 10)||' '||Rpad('Status', 10)||' '||Rpad('Actual Start Date', 22)||' '||Rpad('Actual Completion Date', 22)||' '||Rpad('Action', 50));
                write_log(Rpad('-',10, '-')||' '||Rpad('-',10, '-')||' '||Rpad('-',22, '-')||' '||Rpad('-',22, '-')||' ' ||Rpad('-',50, '-'));

                l_sub_requests := fnd_concurrent.get_sub_requests(l_request_id);
                i := l_sub_requests.first;

                while i is not null loop

                  l_actual_start_date := null;
                  l_actual_completion_date := null;          

                  if (l_sub_requests(i).dev_status = 'NORMAL') then
                      l_normal_count := l_normal_count + 1;
                      l_action_message := 'Completed successfully.';
                  elsif (l_sub_requests(i).dev_status = 'WARNING') then
                      l_warning_count := l_warning_count + 1;
                      l_action_message := 'Warnings reported, please see the sub-request log file.';
                  elsif (l_sub_requests(i).dev_status = 'ERROR') then
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Errors reported, please see the sub-request log file.';
                  else
                      l_sub_requests(i).dev_status := 'ERROR';
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Unknown status reported, please see the sub-request log file.';
                  end if;

                  begin
                  select fcr.actual_start_date, actual_completion_date
                      into l_actual_start_date, l_actual_completion_date
                  from fnd_concurrent_requests    fcr
                  where fcr.request_id = l_sub_requests(i).request_id;
                  exception when no_data_found then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  when others then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  end;
                  
                  write_log(Rpad(l_sub_requests(i).request_id, 10)||' '||Rpad(l_sub_requests(i).dev_status, 10)||' '||Rpad(to_char(l_actual_start_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(to_char(l_actual_completion_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(l_action_message, 50));

                  i := l_sub_requests.next(i);

                end loop;
                
            end if;
        end if;
    end if;
    
    write_log(' ');
    write_log(' - End XXGL_LOAN_SWEEP_PKG.submit_prc');
END;                   

END XXGL_LOAN_SWEEP_PKG;
/
