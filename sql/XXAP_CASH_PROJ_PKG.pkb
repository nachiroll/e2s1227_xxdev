CREATE OR REPLACE PACKAGE BODY APPS.XXAP_CASH_PROJ_PKG AS
  ----------------------------------------------------------------------
  /*
   Package Name    : XXAP_CASH_PROJ_PKG
   Author's Name   : Nattanan P.
   RICEW Object ID : XXAP_CASH_PROJ_PKG

   Date Written    : 04-Apr-2019

   Concurrent Program : XXAR - Cash Projection

   Description :  This Script Creates the Custom Package Spec
                  for provide XXAR - Cash Projection to TMS by CSV format

   Maintenance History :

   Date        Issue# Name                 Remarks
   ----------- ------ -------------------- ------------------------------
   30-Jan-2019      1 Nattanan P.          Initial Development.
  */

PROCEDURE main_prc(o_errbuf  out varchar2,
                   o_retcode out varchar2,
                   --
                   p_user_id      in number,
                   p_resp_id      in number,
                   p_resp_app_id  in number,
                   p_comp_from in varchar2,                   
                   p_comp_to in varchar2,
                   p_prog_short_name in varchar2,
                   p_file_type_frm in varchar2,
                   p_file_type_to in varchar2,
                   p_file_name in varchar2) IS
                                      
cursor c_ar is
select comp.account comp_code, comp.ou_name, comp.currency_code currency, mp_ac.meaning bank_acc, rsa.due_date, sum(acctd_amount_due_remaining) acctd_amount_due_remaining
from ar_payment_schedules_all rsa 
    ,xxar_comp_v comp
    ,(select replace(flvv.lookup_code, '-', '') lookup_code, flvv.meaning
      from fnd_lookup_values_vl flvv
      where flvv.lookup_type = 'XX_CTB_EFT_BANK_AC_BY_OU') mp_ac
where status = 'OP'
and amount_due_remaining <> 0
and comp.ou_id = rsa.org_id
and replace(comp.ou_name, '-', '') = mp_ac.lookup_code(+)
and comp.account between nvl(p_comp_from, comp.account) and nvl(p_comp_to, comp.account)
group by comp.account, comp.ou_name, comp.currency_code, mp_ac.meaning, rsa.due_date
order by 1, 5, 4;               

l_ticketno varchar2(60) := null;
l_instrument varchar2(25) := 'RECEIPT';
l_payment_method varchar2(200) := null;
l_deal_date varchar2(10);
l_value_date varchar2(10);
l_cparty varchar2(35) := 'NONE';
l_entity varchar2(25);
l_strategy varchar2(50) := null;
l_accountnumber varchar2(30);
l_bank varchar2(40);
l_dealer varchar2(40) := null;
l_comments varchar2(255) := null;
l_amt varchar2(50);
l_currency varchar2(3); 

l_err_cnt number := 0;
l_err_line_cnt number;
l_cvt_gl_date varchar2(25);
l_cnt_rec number := 0;
l_err_message varchar2(500) := null;
l_completed_rec number := 0;

BEGIN

    write_log(' - Start XXAP_CASH_PROJ_PKG.main_prc');                
    
    -- Setup Initialize
    -------------------------------------------------------------------------
    apps_initial(l_user_id     => p_user_id, --in number,
                 l_resp_id     => p_resp_id, --in number,
                 l_resp_app_id => p_resp_app_id --in number
                 );

    --Write Header Column
    write_output('TicketNo'|| ',' ||
                 'Instrument'|| ',' ||
                 'Payment Methoid'|| ',' ||
                 'Amount'|| ',' ||
                 'Currency'|| ',' ||
                 'Deal Date'|| ',' ||
                 'Value Date'|| ',' ||
                 'Cparty'|| ',' ||
                 'Entity'|| ',' ||
                 'Strategy'|| ',' ||
                 'AccountNumber'|| ',' ||
                 'Bank'|| ',' ||
                 'Dealer'|| ',' ||
                 'Comments');                                               
    
    for rec_ar in c_ar loop
        
        l_cnt_rec := l_cnt_rec + 1;
        
        l_entity := null;        
        l_accountnumber := null;
        l_bank := null;
        l_amt := null;
        l_currency := null;
        l_err_line_cnt := 0;
                            
        --Validation             
        begin
        select 'ARPROJ'||to_char(trunc(sysdate), 'YYYYMMDD')||'-'||l_cnt_rec
            into l_ticketno
        from dual;
        exception when others then
            l_ticketno := null;
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_ar.ou_name||' Error:- Cannot get Ticket No.');
        end;
        
        if rec_ar.due_date is not null then        
            begin
            select to_char(trunc(rec_ar.due_date)+15, 'DD-MM-YYYY')
                into l_deal_date
            from dual;
            exception when others then
                l_deal_date := null;
                l_err_line_cnt := l_err_line_cnt + 1;
                write_log(rec_ar.due_date||' Error:- Cannot convert Due Date to format DD-MM-YYYY');                 
            end;
            
            l_value_date := l_deal_date;
        
            l_entity := rec_ar.comp_code;
            
        else
            l_deal_date := null;
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_ar.ou_name||' Error:- Due Date is null');
        end if;                
                                               
        if rec_ar.bank_acc is not null then
            
            l_accountnumber := rec_ar.bank_acc;
            
            begin
            select cbb.bank_name
                into l_bank
            from ce_bank_accounts cba, ce_bank_branches_v cbb
            where cba.bank_account_num = '0'||l_accountnumber
            and cbb.branch_party_id = cba.bank_branch_id
            and cbb.bank_party_id = cba.bank_id;
            exception when others then
                l_bank := null;
                l_err_line_cnt := l_err_line_cnt + 1;
                write_log(l_accountnumber||' Error:- Not found bank name'); 
            end;
                        
        else
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_ar.ou_name||' Error:- Not found bank account in XX_CTB_EFT_BANK_AC_BY_OU');           
        end if;
        
        if rec_ar.acctd_amount_due_remaining is not null then
            begin
            select nvl(substr(round(rec_ar.acctd_amount_due_remaining,2), 1, instr(round(rec_ar.acctd_amount_due_remaining,2),'.')-1),0)||'.'||
                   rpad(nvl(substr(round(rec_ar.acctd_amount_due_remaining,2), instr(round(rec_ar.acctd_amount_due_remaining,2),'.')+1),0), 2, 0) dr_amt
                into l_amt        
            from dual;
            exception when others then
                l_amt := '0.00';                    
                l_err_line_cnt := l_err_line_cnt + 1;
                write_log(rec_ar.acctd_amount_due_remaining||' Error:- Cannot convert number format N.NN');
            end;
        else 
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_ar.ou_name||' Error:- Amount Due Remaining is null');
        end if;
        
        if rec_ar.currency is not null then
            
            l_currency := rec_ar.currency;
                        
        else
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_ar.ou_name||' Error:- Currency is null');
        end if;
        
        if l_err_line_cnt = 0 then
            --Write Header Column
            write_output(substr(l_ticketno,1,60)|| ',' ||
                         substr(l_instrument,1,25)|| ',' ||
                         substr(l_payment_method,1,200)|| ',' ||
                         l_amt|| ',' ||
                         substr(l_currency,1,3)|| ',' ||
                         substr(l_deal_date,1,10)|| ',' ||
                         substr(l_value_date,1,10)|| ',' ||
                         substr(l_cparty,1,35)|| ',' ||
                         substr(l_entity,1,25)|| ',' ||
                         substr(l_strategy,1,50)|| ',' ||
                         substr(l_accountnumber,1,30)|| ',' ||
                         substr(l_bank,1,40)|| ',' ||
                         substr(l_dealer,1,40)|| ',' ||
                         substr(l_comments,1,3));
                                  
            l_completed_rec := l_completed_rec + 1;
                        
        else
            l_err_cnt := l_err_cnt + 1;
        end if;
                                    
    end loop;                            
                                                                                    
    write_log('Total records is '||l_cnt_rec);    
    write_log('Completed records is '||l_completed_rec);
    write_log('Rejected records is '||l_err_cnt);
            
    write_log(' - End XXAP_CASH_PROJ_PKG.main_prc');
    
END main_prc;

PROCEDURE apps_initial(l_user_id     in number,
                       l_resp_id     in number,
                       l_resp_app_id in number) is
--
BEGIN
write_log(' - Run Application Initialize.');

APPS.FND_GLOBAL.apps_initialize(user_id      => l_user_id, --in number,
                                resp_id      => l_resp_id, --in number,
                                resp_appl_id => l_resp_app_id --in number,
                                );

EXCEPTION WHEN OTHERS THEN
    write_log(' - ***** Error While Application Initialize *****');
    write_log(SQLERRM);
END apps_initial;

PROCEDURE write_output(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.output,l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_output : ' || sqlerrm);         
END write_output;

PROCEDURE write_log(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.log,to_char(sysdate, 'HH24:MI:SS') || '  ' || l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_log : ' || sqlerrm);         
END WRITE_LOG;

PROCEDURE submit_prc(o_errbuf  out varchar2,
                     o_retcode out varchar2,
                     --
                     p_user_id      in number,
                     p_resp_id      in number,
                     p_resp_app_id  in number,
                     p_comp_from in varchar2,                   
                     p_comp_to in varchar2,
                     p_prog_short_name in varchar2,
                     p_file_type_frm in varchar2,
                     p_file_type_to in varchar2,
                     p_file_name in varchar2) IS
l_request_id number;
l_request_data varchar2(100);
l_appl_code varchar2(150) := 'XXDEV';
l_prog_code varchar2(150) := 'XXGL_TMS_MV_CSVFILE';
l_sub_request_id_out number;
l_sub_request_id_csv number;
l_request_count number;
l_phase_out varchar2(50);
l_status_out varchar2(50);
l_dev_phase_out varchar2(50);
l_dev_status_out varchar2(50);
l_message_out varchar2(50);
l_req_return_status_out boolean;
l_normal_count number := 0;
l_warning_count number := 0;  
l_error_count number := 0;
l_sub_requests fnd_concurrent.requests_tab_type;
i number;
l_actual_start_date date;
l_actual_completion_date date;
l_action_message VARCHAR2(200);

l_err_cnt number :=0;
l_cnt_data number := 0;
l_sys_date varchar2(50);


BEGIN
    write_log(' - Start XXAP_CASH_PROJ_PKG.submit_prc');
    
    begin
    select to_char(trunc(sysdate), 'YYYY/MM/DD HH24:MI:SS')
        into l_sys_date
    from dual;
    exception when others then
        l_sys_date := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- Cannot convert system date format');
    end;
                
    begin
    select count(*)
        into l_cnt_data
    from ar_payment_schedules_all rsa 
    ,xxar_comp_v comp
    ,(select replace(flvv.lookup_code, '-', '') lookup_code, flvv.meaning
      from fnd_lookup_values_vl flvv
      where flvv.lookup_type = 'XX_CTB_EFT_BANK_AC_BY_OU') mp_ac
    where status = 'OP'
    and amount_due_remaining <> 0
    and comp.ou_id = rsa.org_id
    and replace(comp.ou_name, '-', '') = mp_ac.lookup_code(+)
    and comp.account between nvl(p_comp_from, comp.account) and nvl(p_comp_to, comp.account);
    exception when others then
        l_cnt_data := 0;
    end;
    
    if l_cnt_data = 0 then
        write_log(' - ***** No data found *****');
    else
        if l_err_cnt = 0 then         
            l_request_id :=  fnd_global.conc_request_id;
            l_request_data := fnd_conc_global.request_data;
            l_request_count := 0;        
                
            if (l_request_data is null) then
                
                write_log('DEBUG: Program submition list');
                write_log(Rpad('Program Name', 50)||' '||Rpad('Request ID', 15));
                write_log(Rpad('-',50, '-')||' '||Rpad('-',15, '-'));    
            
                l_request_count := l_request_count + 1;
                             
                l_sub_request_id_out := fnd_request.submit_request(application => l_appl_code,
                                                                   program     => p_prog_short_name,
                                                                   description => null,
                                                                   start_time  => null,
                                                                   sub_request => true,
                                                                   argument1   => p_user_id,
                                                                   argument2   => p_resp_id,
                                                                   argument3   => p_resp_app_id,
                                                                   argument4   => p_comp_from,
                                                                   argument5   => p_comp_to,
                                                                   argument6   => p_prog_short_name,
                                                                   argument7   => p_file_type_frm,
                                                                   argument8   => p_file_type_to,
                                                                   argument9   => p_file_name);
            
                commit;
              
                if (l_sub_request_id_out = 0) then
                    write_log(Rpad(p_prog_short_name, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                else
                    write_log(Rpad(p_prog_short_name, 50)||' '||l_sub_request_id_out);          
                end if;        
                    
                if l_sub_request_id_out is not null then
                                                                                                                                                                         
                    l_request_count := l_request_count + 1;
                            
                    l_sub_request_id_csv := fnd_request.submit_request(application => l_appl_code,
                                                                       program     => l_prog_code,
                                                                       description => null,
                                                                       start_time  => null,
                                                                       sub_request => true,
                                                                       argument1   => l_sub_request_id_out,
                                                                       argument2   => l_sys_date,
                                                                       argument3   => p_prog_short_name,
                                                                       argument4   => p_file_type_frm,
                                                                       argument5   => p_file_type_to,
                                                                       argument6   => p_file_name);
                    commit;
                            
                    if (l_sub_request_id_csv = 0) then
                        write_log(Rpad(l_prog_code, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                    else
                        write_log(Rpad(l_prog_code, 50)||' '||l_sub_request_id_csv);          
                    end if;
                    
                end if;            

                l_request_count := Nvl(l_request_count, 0);
                fnd_conc_global.set_req_globals(conc_status  => 'PAUSED',request_data => To_char(l_request_count));
            else
                write_log(' ');
                write_log('Program Status');
                write_log(Rpad('-',118,'-'));
                write_log(Rpad('Request ID', 10)||' '||Rpad('Status', 10)||' '||Rpad('Actual Start Date', 22)||' '||Rpad('Actual Completion Date', 22)||' '||Rpad('Action', 50));
                write_log(Rpad('-',10, '-')||' '||Rpad('-',10, '-')||' '||Rpad('-',22, '-')||' '||Rpad('-',22, '-')||' ' ||Rpad('-',50, '-'));

                l_sub_requests := fnd_concurrent.get_sub_requests(l_request_id);
                i := l_sub_requests.first;

                while i is not null loop

                  l_actual_start_date := null;
                  l_actual_completion_date := null;          

                  if (l_sub_requests(i).dev_status = 'NORMAL') then
                      l_normal_count := l_normal_count + 1;
                      l_action_message := 'Completed successfully.';
                  elsif (l_sub_requests(i).dev_status = 'WARNING') then
                      l_warning_count := l_warning_count + 1;
                      l_action_message := 'Warnings reported, please see the sub-request log file.';
                  elsif (l_sub_requests(i).dev_status = 'ERROR') then
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Errors reported, please see the sub-request log file.';
                  else
                      l_sub_requests(i).dev_status := 'ERROR';
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Unknown status reported, please see the sub-request log file.';
                  end if;

                  begin
                  select fcr.actual_start_date, actual_completion_date
                      into l_actual_start_date, l_actual_completion_date
                  from fnd_concurrent_requests    fcr
                  where fcr.request_id = l_sub_requests(i).request_id;
                  exception when no_data_found then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  when others then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  end;
                  
                  write_log(Rpad(l_sub_requests(i).request_id, 10)||' '||Rpad(l_sub_requests(i).dev_status, 10)||' '||Rpad(to_char(l_actual_start_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(to_char(l_actual_completion_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(l_action_message, 50));

                  i := l_sub_requests.next(i);

                end loop;
                
            end if;
        end if;
    end if;
    
    write_log(' ');
    write_log(' - End XXAP_CASH_PROJ_PKG.submit_prc');
END;                   

END XXAP_CASH_PROJ_PKG;
/
