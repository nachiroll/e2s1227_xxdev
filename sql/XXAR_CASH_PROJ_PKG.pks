CREATE OR REPLACE PACKAGE APPS.XXAR_CASH_PROJ_PKG AS
  ----------------------------------------------------------------------
  /*
   Package Name    : XXAR_CASH_PROJ_PKG
   Author's Name   : Nattanan P.
   RICEW Object ID : XXAR_CASH_PROJ_PKG

   Date Written    : 30-Jan-2019

   Concurrent Program : XXAR - Cash Projection

   Description :  This Script Creates the Custom Package Spec
                  for provide XXAR - Cash Projection to TMS by CSV format

   Maintenance History :

   Date        Issue# Name                 Remarks
   ----------- ------ -------------------- ------------------------------
   30-Jan-2019      1 Nattanan P.          Initial Development.
  */

PROCEDURE main_prc(o_errbuf  out varchar2,
                   o_retcode out varchar2,
                   --
                   p_user_id      in number,
                   p_resp_id      in number,
                   p_resp_app_id  in number,
                   p_comp_from in varchar2,                   
                   p_comp_to in varchar2,
                   p_prog_short_name in varchar2,
                   p_file_type_frm in varchar2,
                   p_file_type_to in varchar2,
                   p_file_name in varchar2);
PROCEDURE apps_initial(l_user_id     in number,
                       l_resp_id     in number,
                       l_resp_app_id in number);
                     
PROCEDURE write_output(l_message in varchar2);

PROCEDURE write_log(l_message in varchar2);

PROCEDURE submit_prc (o_errbuf  out varchar2,
                      o_retcode out varchar2,
                      --
                      p_user_id      in number,
                      p_resp_id      in number,
                      p_resp_app_id  in number,
                      p_comp_from in varchar2,                   
                      p_comp_to in varchar2,
                      p_prog_short_name in varchar2,
                      p_file_type_frm in varchar2,
                      p_file_type_to in varchar2,
                      p_file_name in varchar2
                      );                                          

END XXAR_CASH_PROJ_PKG;
/
