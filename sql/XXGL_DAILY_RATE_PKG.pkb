CREATE OR REPLACE PACKAGE BODY APPS.XXGL_DAILY_RATE_PKG AS
  ----------------------------------------------------------------------
  /*
   Package Name    : XXGL_DAILY_RATE_PKG
   Author's Name   : Nattanan P.
   RICEW Object ID : XXGL_DAILY_RATE_PKG

   Date Written    : 30-Jan-2019

   Concurrent Program : XXGL - GL Daily Rate

   Description :  This Script Creates the Custom Package Spec
                  for provide GL Daily Rate to TMS by CSV format

   Maintenance History :

   Date        Issue# Name                 Remarks
   ----------- ------ -------------------- ------------------------------
   30-Jan-2019      1 Nattanan P.          Initial Development.
  */

PROCEDURE main_prc(o_errbuf  out varchar2,
                   o_retcode out varchar2,
                   --
                   p_user_id      in number,
                   p_resp_id      in number,
                   p_resp_app_id  in number,
                   p_curr_from in varchar2,                   
                   p_as_of_date in varchar2,
                   p_prog_short_name in varchar2,
                   p_file_type_frm in varchar2,
                   p_file_type_to in varchar2,
                   p_file_name in varchar2) IS

cursor c_cur_dr (p_conversion_date date) is
select distinct primary.from_currency from_currency, 
                primary.to_currency to_currency, 
                primary.conversion_date conversion_date
from gl_daily_rates secondary,
     gl_daily_rates primary,
     gl_daily_conversion_types ct
where secondary.to_currency = primary.from_currency || ''
and secondary.from_currency = primary.to_currency || ''
and secondary.conversion_date = primary.conversion_date + 0
and secondary.conversion_type = primary.conversion_type || ''
and ct.conversion_type = primary.conversion_type || ''
and ct.user_conversion_type in ('BOT Buying Rate', 'BOT Selling Rate')
and primary.from_currency = p_curr_from
and primary.conversion_date = p_conversion_date
order by primary.from_currency, primary.to_currency, primary.conversion_date;                   

l_buy_rate varchar2(100);
l_sel_rate varchar2(100);

l_conversion_date date;
l_err_cnt number :=0;
l_err_line_cnt number;
l_cvt_date varchar2(25);
l_cnt_rec number :=0;
l_err_message varchar2(500) := null;
l_completed_rec number :=0;


BEGIN

    write_log(' - Start XXGL_DAILY_RATE_PKG.main_prc');                
    
    -- Setup Initialize
    -------------------------------------------------------------------------
    apps_initial(l_user_id     => p_user_id, --in number,
                 l_resp_id     => p_resp_id, --in number,
                 l_resp_app_id => p_resp_app_id --in number
                 );
        
    l_conversion_date := p_as_of_date;           
            
    --Write Header Column
    write_output('Currency_From'|| ',' ||
                 'Currency_To'|| ',' ||
                 'Conversion_Date'|| ',' ||
                 'BOT Buying Rate'|| ',' ||
                 'BOT Selling Rate');            
                       
    for rec_cur_dr in c_cur_dr (l_conversion_date) loop
        l_buy_rate := null;    
        l_sel_rate := null;        
        l_cnt_rec := l_cnt_rec + 1;
        l_err_line_cnt := 0;
                            
        --Validation                                            
        if rec_cur_dr.conversion_date is not null then
                                                                            
            begin
            select to_char(rec_cur_dr.conversion_date, 'DD-Mon-YYYY')
                into l_cvt_date
            from dual;                
            exception when others then
                l_cvt_date := null;   
                l_err_line_cnt := l_err_line_cnt + 1;
                write_log(rec_cur_dr.from_currency||' - '||rec_cur_dr.to_currency||' Error:- Cannot convert conversion date to format DD-Mon-YYYY');              
            end;
                         
        end if;
                            
        --Get BOT Buying Rate
        begin
        select nvl(substr(round(primary.conversion_rate,6), 1, instr(round(primary.conversion_rate,6),'.')-1),0)||'.'||
               rpad(nvl(substr(round(primary.conversion_rate,6), instr(round(primary.conversion_rate,6),'.')+1),0), 6, 0) conversion_rate
            into l_buy_rate
        from gl_daily_rates secondary,
             gl_daily_rates primary,
             gl_daily_conversion_types ct
        where secondary.to_currency = primary.from_currency || ''
        and secondary.from_currency = primary.to_currency || ''
        and secondary.conversion_date = primary.conversion_date + 0
        and secondary.conversion_type = primary.conversion_type || ''
        and ct.conversion_type = primary.conversion_type || ''
        and ct.user_conversion_type = 'BOT Buying Rate'
        and primary.from_currency = rec_cur_dr.from_currency
        and primary.to_currency = rec_cur_dr.to_currency
        and primary.conversion_date = rec_cur_dr.conversion_date;
        exception when no_data_found then
            l_buy_rate := '0.000000';
        when others then
            l_buy_rate := '0.000000';
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_cur_dr.from_currency||' - '||rec_cur_dr.to_currency||' Error:- Cannot get BOT Buying Rate');
        end;
                            
        --Get BOT Selling Rate
        begin
        select nvl(substr(round(primary.conversion_rate,6), 1, instr(round(primary.conversion_rate,6),'.')-1),0)||'.'||
               rpad(nvl(substr(round(primary.conversion_rate,6), instr(round(primary.conversion_rate,6),'.')+1),0), 6, 0) conversion_rate
            into l_sel_rate
        from gl_daily_rates secondary,
             gl_daily_rates primary,
             gl_daily_conversion_types ct
        where secondary.to_currency = primary.from_currency || ''
        and secondary.from_currency = primary.to_currency || ''
        and secondary.conversion_date = primary.conversion_date + 0
        and secondary.conversion_type = primary.conversion_type || ''
        and ct.conversion_type = primary.conversion_type || ''
        and ct.user_conversion_type = 'BOT Selling Rate'
        and primary.from_currency = rec_cur_dr.from_currency
        and primary.to_currency = rec_cur_dr.to_currency
        and primary.conversion_date = rec_cur_dr.conversion_date;
        exception when no_data_found then
            l_sel_rate := '0.000000';
        when others then
            l_sel_rate := '0.000000';
            l_err_line_cnt := l_err_line_cnt + 1;
            write_log(rec_cur_dr.from_currency||' - '||rec_cur_dr.to_currency||' Error:- Cannot get BOT Selling Rate');
        end;
                        
        if l_err_line_cnt = 0 then
            --Write Header Column
            write_output(substr(rec_cur_dr.from_currency,1,3)|| ',' ||
                         substr(rec_cur_dr.to_currency,1,3)|| ',' ||
                         substr(l_cvt_date,1,11)|| ',' ||
                         l_buy_rate|| ',' ||
                         l_sel_rate);
                        
            l_completed_rec := l_completed_rec + 1;
        
        else
            l_err_cnt := l_err_cnt + 1;                
        end if;
                                                                                       
    end loop;                            

                                                                                    
    write_log('Total records is '||l_cnt_rec);    
    write_log('Completed records is '||l_completed_rec);
    write_log('Rejected records is '||l_err_cnt);
            
    write_log(' - End XXGL_DAILY_RATE_PKG.main_prc');
    
END main_prc;

PROCEDURE apps_initial(l_user_id     in number,
                       l_resp_id     in number,
                       l_resp_app_id in number) is
--
BEGIN
write_log(' - Run Application Initialize.');

APPS.FND_GLOBAL.apps_initialize(user_id      => l_user_id, --in number,
                                resp_id      => l_resp_id, --in number,
                                resp_appl_id => l_resp_app_id --in number,
                                );

EXCEPTION WHEN OTHERS THEN
    write_log(' - ***** Error While Application Initialize *****');
    write_log(SQLERRM);
END apps_initial;

PROCEDURE write_output(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.output,l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_output : ' || sqlerrm);         
END write_output;

PROCEDURE write_log(l_message in varchar2)
IS
BEGIN
    fnd_file.put_line(apps.fnd_file.log,to_char(sysdate, 'HH24:MI:SS') || '  ' || l_message);    
EXCEPTION WHEN OTHERS THEN
         fnd_file.put_line(apps.fnd_file.log,'Error write_log : ' || sqlerrm);         
END WRITE_LOG;

PROCEDURE submit_prc(o_errbuf  out varchar2,
                     o_retcode out varchar2,
                     --
                     p_user_id      in number,
                     p_resp_id      in number,
                     p_resp_app_id  in number,
                     p_curr_from in varchar2,                   
                     p_as_of_date in varchar2,
                     p_prog_short_name in varchar2,
                     p_file_type_frm in varchar2,
                     p_file_type_to in varchar2,
                     p_file_name in varchar2) IS
l_request_id number;
l_request_data varchar2(100);
l_appl_code varchar2(150) := 'XXDEV';
l_prog_code varchar2(150) := 'XXGL_TMS_MV_CSVFILE';
l_sub_request_id_out number;
l_sub_request_id_csv number;
l_request_count number;
l_phase_out varchar2(50);
l_status_out varchar2(50);
l_dev_phase_out varchar2(50);
l_dev_status_out varchar2(50);
l_message_out varchar2(50);
l_req_return_status_out boolean;
l_normal_count number := 0;
l_warning_count number := 0;  
l_error_count number := 0;
l_sub_requests fnd_concurrent.requests_tab_type;
i number;
l_actual_start_date date;
l_actual_completion_date date;
l_action_message VARCHAR2(200);

l_conversion_date date;
l_cnt_data number :=0;
l_err_cnt number := 0;
l_as_of_date varchar2(25);

BEGIN
    write_log(' - Start XXGL_DAILY_RATE_PKG.submit_prc');
    
    begin
    select trunc(to_date(p_as_of_date, 'YYYY/MM/DD HH24:MI:SS')) - 1 conversion_date
        into l_conversion_date
    from dual;
    exception when others then
        l_conversion_date := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- Cannot convert parameter as of date format');
    end;
    
    begin
    select to_char(to_date(l_conversion_date),'YYYY/MM/DD HH24:MI:SS')
        into l_as_of_date
    from dual;
    exception when others then
        l_as_of_date := null;
        l_err_cnt := l_err_cnt + 1;
        write_log('Error:- Cannot convert as of date - 1 end date format');
    end;
    
    begin
    select count(*)
        into l_cnt_data
    from gl_daily_rates secondary,
         gl_daily_rates primary,
         gl_daily_conversion_types ct
    where secondary.to_currency = primary.from_currency || ''
    and secondary.from_currency = primary.to_currency || ''
    and secondary.conversion_date = primary.conversion_date + 0
    and secondary.conversion_type = primary.conversion_type || ''
    and ct.conversion_type = primary.conversion_type || ''
    and ct.user_conversion_type in ('BOT Buying Rate', 'BOT Selling Rate')
    and primary.from_currency = p_curr_from
    and primary.conversion_date = l_conversion_date;
    exception when others then
        l_cnt_data := 0;
    end;
    
    if l_cnt_data = 0 then
        write_log(' - ***** No data found *****');
    else
        if l_err_cnt = 0 then    
            l_request_id :=  fnd_global.conc_request_id;
            l_request_data := fnd_conc_global.request_data;
            l_request_count := 0;
                
            if (l_request_data is null) then
                
                write_log('Program submition list');
                write_log(Rpad('Program Name', 50)||' '||Rpad('Request ID', 15));
                write_log(Rpad('-',50, '-')||' '||Rpad('-',15, '-'));    
            
                l_request_count := l_request_count + 1;
                             
                l_sub_request_id_out := fnd_request.submit_request(application => l_appl_code,
                                                                   program     => p_prog_short_name,
                                                                   description => null,
                                                                   start_time  => null,
                                                                   sub_request => true,
                                                                   argument1   => p_user_id,
                                                                   argument2   => p_resp_id,
                                                                   argument3   => p_resp_app_id,
                                                                   argument4   => p_curr_from,
                                                                   argument5   => l_conversion_date,
                                                                   argument6   => p_prog_short_name,
                                                                   argument7   => p_file_type_frm,
                                                                   argument8   => p_file_type_to,
                                                                   argument9   => p_file_name);
            
                commit;
              
                if (l_sub_request_id_out = 0) then
                    write_log(Rpad(p_prog_short_name, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                else
                    write_log(Rpad(p_prog_short_name, 50)||' '||l_sub_request_id_out);          
                end if;        
                    
                if l_sub_request_id_out is not null then
                                                                                                                                                 
                    l_request_count := l_request_count + 1;
                            
                    l_sub_request_id_csv := fnd_request.submit_request(application => l_appl_code,
                                                                       program     => l_prog_code,
                                                                       description => null,
                                                                       start_time  => null,
                                                                       sub_request => true,
                                                                       argument1   => l_sub_request_id_out,
                                                                       argument2   => l_as_of_date,
                                                                       argument3   => p_prog_short_name,
                                                                       argument4   => p_file_type_frm,
                                                                       argument5   => p_file_type_to,
                                                                       argument6   => p_file_name);
                    commit;
                            
                    if (l_sub_request_id_csv = 0) then
                        write_log(Rpad(l_prog_code, 50)||' '||'ERROR: Unable to submit '||fnd_message.get);
                    else
                        write_log(Rpad(l_prog_code, 50)||' '||l_sub_request_id_csv);          
                    end if;
                    
                end if;            

                l_request_count := Nvl(l_request_count, 0);
                fnd_conc_global.set_req_globals(conc_status  => 'PAUSED',request_data => To_char(l_request_count));
            else
                write_log(' ');
                write_log('Program Status');
                write_log(Rpad('-',118,'-'));
                write_log(Rpad('Request ID', 10)||' '||Rpad('Status', 10)||' '||Rpad('Actual Start Date', 22)||' '||Rpad('Actual Completion Date', 22)||' '||Rpad('Action', 50));
                write_log(Rpad('-',10, '-')||' '||Rpad('-',10, '-')||' '||Rpad('-',22, '-')||' '||Rpad('-',22, '-')||' ' ||Rpad('-',50, '-'));

                l_sub_requests := fnd_concurrent.get_sub_requests(l_request_id);
                i := l_sub_requests.first;

                while i is not null loop

                  l_actual_start_date := null;
                  l_actual_completion_date := null;          

                  if (l_sub_requests(i).dev_status = 'NORMAL') then
                      l_normal_count := l_normal_count + 1;
                      l_action_message := 'Completed successfully.';
                  elsif (l_sub_requests(i).dev_status = 'WARNING') then
                      l_warning_count := l_warning_count + 1;
                      l_action_message := 'Warnings reported, please see the sub-request log file.';
                  elsif (l_sub_requests(i).dev_status = 'ERROR') then
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Errors reported, please see the sub-request log file.';
                  else
                      l_sub_requests(i).dev_status := 'ERROR';
                      l_error_count := l_error_count + 1;
                      l_action_message := 'Unknown status reported, please see the sub-request log file.';
                  end if;

                  begin
                  select fcr.actual_start_date, actual_completion_date
                      into l_actual_start_date, l_actual_completion_date
                  from fnd_concurrent_requests    fcr
                  where fcr.request_id = l_sub_requests(i).request_id;
                  exception when no_data_found then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  when others then
                      l_actual_start_date := null;
                      l_actual_completion_date := null;
                  end;
                  
                  write_log(Rpad(l_sub_requests(i).request_id, 10)||' '||Rpad(l_sub_requests(i).dev_status, 10)||' '||Rpad(to_char(l_actual_start_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(to_char(l_actual_completion_date, 'DD-Mon-YY HH24:MI:SS'), 22)||' '||Rpad(l_action_message, 50));

                  i := l_sub_requests.next(i);

                end loop;
                
            end if;
        end if;
    end if;
    write_log(' ');
    write_log(' - End XXGL_DAILY_RATE_PKG.submit_prc');
END;                   

END XXGL_DAILY_RATE_PKG;
/
